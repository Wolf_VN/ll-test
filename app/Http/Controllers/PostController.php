<?php

namespace App\Http\Controllers;

use App\Transformers\PostDetailTransformer;
use App\Transformers\PostTransformer;
use App\Utils\Cache\PostCache;
use Illuminate\Http\Request;
use App\Repositories\EloquentPost;
use Illuminate\Http\Response as IlluminateResponse;
use App\Utils\SendMail;

class PostController extends Controller
{
    protected $mainEloquent;

    protected $mainTransformer;

    protected $postCache;

    function __construct(EloquentPost $eloquentPost, PostTransformer $postTransformer, PostCache $postCache)
    {
        $this->mainEloquent = $eloquentPost;
        $this->mainTransformer = $postTransformer;
        $this->postCache = $postCache;
    }

    public function index(PostCache $postCache)
    {
        $records = [];
        if ($ids = $postCache->getListPosts()) {
            foreach ($ids as $id) {
                $records[] = $postCache->keyGet($id);
            }
        }

        return $this->response->collection(collect($records), $this->mainTransformer);
    }

    public function store(Request $request, PostCache $postCache)
    {
        $data = [
            'title' => $request->get('title', ''),
            'body'  => $request->get('body', ''),
            'tags'  => $request->get('tags', [])
        ];

        try {
            if ($createdPost = $this->mainEloquent->create($data)) {
                // send mail
                $this->notifyPostCreated($createdPost->toArray());
                // build cache
                // todo: need to refactory
                $postCache->buildOnePost($createdPost);

                return $this->response->item($createdPost, $this->mainTransformer)
                                      ->setStatusCode(IlluminateResponse::HTTP_CREATED);
            }

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest('Occur Error, please try again.');
    }

    public function update($id, Request $request, PostCache $postCache)
    {
        $data = [
            'id'    => $id,
            'title' => $request->get('title', ''),
            'body'  => $request->get('body', ''),
            'tags'  => $request->get('tags', [])
        ];

        try {
            if ( !$post = $postCache->keyGet($id)) {
                throw new \Exception('ID is invalid');
            }

            if ($updatedPost = $this->mainEloquent->update($data)) {
                // build cache
                // todo: need to refactory
                $postCache->buildOnePost($updatedPost);

                return $this->response->item($updatedPost, $this->mainTransformer);
            }
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest('Occur Error, please try again.');
    }

    public function view($id, PostCache $postCache, PostDetailTransformer $postDetailTransformer)
    {
        $post = $postCache->keyGet($id);

        return $this->response->item($post, $postDetailTransformer);
    }

    public function delete($id)
    {
        if ( !$post = $this->mainEloquent->getById($id, 'tags')) {
            throw new \Exception('ID is invalid');
        }

        $result = $this->mainEloquent->deleteById($id);

        if ($result) {
            // todo: implement Log

            return $this->response->noContent();
        }

        return $this->response->errorBadRequest('Occur error, cannot delete');
    }

    private function notifyPostCreated($data)
    {
        $sendMail = new SendMail();
        $sendMail->pushToQueueMail('mail.created_post',
                            config('mail.admin'),
                            "New Post [ID: {$data['id']}] has just created.",
                            $data);
    }
}
