<?php

namespace App\Http\Controllers;

use App\Transformers\PostTransformer;
use App\Transformers\TagTransformer;
use App\Utils\Cache\PostCache;
use App\Utils\Cache\TagCache;
use Illuminate\Http\Request;
use App\Repositories\EloquentTag;
use Illuminate\Http\Response as IlluminateResponse;

class TagController extends Controller
{
    protected $mainEloquent;

    protected $mainTransformer;

    function __construct(EloquentTag $eloquentTag, TagTransformer $tagTransformer)
    {
        $this->mainEloquent = $eloquentTag;
        $this->mainTransformer = $tagTransformer;
    }

    public function index()
    {
        $records = $this->mainEloquent->all();

        return $this->response->collection($records, $this->mainTransformer);
    }

    public function store(Request $request)
    {
        $data = [
            'name' => $request->get('name', ''),
        ];

        try {
            $tag = $this->mainEloquent->create($data);

            return $this->response->item($tag, $this->mainTransformer)
                                  ->setStatusCode(IlluminateResponse::HTTP_CREATED);
            
        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }
        
        return $this->response->errorBadRequest('Occur Error, please try again.');
    }

    public function update($id, Request $request)
    {
        $data = [
            'id'    => $id,
            'name'  => $request->get('name', ''),
        ];

        try {
            $tag = $this->mainEloquent->update($data);

            return $this->response->item($tag, $this->mainTransformer);

        } catch (\Exception $e) {
            return $this->response->errorBadRequest($e->getMessage());
        }

        return $this->response->errorBadRequest('Occur Error, please try again.');
    }

    public function view($id)
    {
        $tag = $this->mainEloquent->view($id);

        return $this->response->item($tag, $this->mainTransformer);
    }

    public function delete($id)
    {
        $result = $this->mainEloquent->deleteById($id);

        if ($result) {
            return $this->response->noContent();
        }

        return $this->response->errorBadRequest('Occur error, cannot delete');
    }

    public function listIntersectPostByTags(
        Request $request,
        TagCache $tagCache,
        PostCache $postCache,
        PostTransformer $postTransformer
    ) {
        $tagIds = $request->get('tags', []);

        $posts = [];
        if ($postIds = $tagCache->listAllIntersectPostsByManyTag($tagIds)) {
            foreach ($postIds as $postId) {
                if ($post = $postCache->keyGet($postId)) {
                    $posts[] = $post;
                }
            }
        }

        return $this->response->collection(collect($posts), $postTransformer);
    }

    public function countInterPostByTags(Request $request, TagCache $tagCache)
    {
        $tagIds = $request->get('tags', []);

        $result = $tagCache->countIntersectPostByTagIds($tagIds);

        return $this->response->array(['data' => ['num_of' => $result]]);
    }
}
