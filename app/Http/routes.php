<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {

	$api->group(['namespace' => 'App\Http\Controllers'], function($api) {

        // Resource Post
        $api->get('/posts', 'PostController@index');
        $api->post('/posts', 'PostController@store');
        $api->get('/posts/{id}', 'PostController@view');
        $api->put('/posts/{id}', 'PostController@update');
        $api->delete('/posts/{id}', 'PostController@delete');

        // Resource Tag
        $api->get('/tags', 'TagController@index');
        $api->post('/tags', 'TagController@store');
        $api->get('/tags/{id}', 'TagController@view')
                ->where('id', '[0-9]+');
        $api->put('/tags/{id}', 'TagController@update')
                ->where('id', '[0-9]+');
        $api->delete('/tags/{id}', 'TagController@delete')
                ->where('id', '[0-9]+');

        $api->get('/tags/list-inter-posts', 'TagController@listIntersectPostByTags');
        $api->get('/tags/count-inter-posts', 'TagController@countInterPostByTags');

	});

});
