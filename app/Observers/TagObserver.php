<?php

namespace App\Observers;

use Illuminate\Database\Eloquent\Model;
use App\Jobs\PostJob;
use Illuminate\Foundation\Bus\DispatchesJobs;

class TagObserver extends AbstractObserver implements ContractObserver
{
    use DispatchesJobs;

    public function deleted(Model $model)
    {
        // postJob to refresh PostCache
        $this->rebuildCacheForPost($model);
        // remove Tag cache
        $this->tagCache->removeTag($model->id);
    }

    public function saved(Model $model)
    {
        if ($model->getOriginal() && $model->isDirty('name')) {
            // postJob to refresh PostCache
            $this->rebuildCacheForPost($model);
        }
    }
    
    private function rebuildCacheForPost(Model $model)
    {
        if ($postIds = $this->tagCache->listAllIntersectPostsByManyTag($model->id)) {
            // postJob to refresh PostCache
            $job = (new PostJob($postIds))
                ->onQueue(config('queue.service_queue.q_build_cache'));

            $this->dispatch($job);
        }
    }
}