<?php

namespace App\Observers;


abstract class AbstractObserver
{
    /**
     * @var \App\Utils\Cache\PostCache
     */
    protected $postCache;

    /**
     * @var \App\Utils\Cache\TagCache
     */
    protected $tagCache;

    /**
     * @var \App\Repositories\EloquentPost
     */
    protected $eloquentPost;


    function __construct()
    {
        $this->postCache    = \App::make('App\Utils\Cache\PostCache');
        $this->tagCache     = \App::make('App\Utils\Cache\TagCache');
        $this->eloquentPost = \App::make('App\Repositories\EloquentPost');
    }
}