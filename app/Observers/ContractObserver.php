<?php

namespace App\Observers;


use Illuminate\Database\Eloquent\Model;

interface ContractObserver
{
    public function saved(Model $model);
    public function deleted(Model $model);
}