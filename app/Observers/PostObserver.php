<?php

namespace App\Observers;

use Illuminate\Database\Eloquent\Model;

class PostObserver extends AbstractObserver implements ContractObserver
{
    public function deleted(Model $model)
    {
        $this->cleanPostOutTag($model);
        $this->postCache->delete($model->id);
    }

    public function saving(Model $model)
    {
        // for updating
        if ($model->id) {
            $this->cleanPostOutTag($model);
        }
    }

    public function saved(Model $model)
    {
//        $this->postCache->buildOnePost($model);
    }

    private function cleanPostOutTag(Model $model)
    {
        if ($model->tags->count()) {
            $tagIds = $model->tags->pluck('id');
            $this->tagCache->removePostOutTag($model->id, $tagIds);
        }
    }
}