<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

class TagTransformer extends TransformerAbstract
{
    public function transform($tag)
    {
        return [
            'id' => $tag->id,
            'name' => $tag->name,
        ];
    }
}