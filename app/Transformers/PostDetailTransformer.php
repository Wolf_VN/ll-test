<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

class PostDetailTransformer extends TransformerAbstract
{
    public function transform($post)
    {
        $tags = [];
        if ( !empty($post) && isset($post->tags)) {
            foreach ($post->tags as $tag) {
                $tags[] = [
                    'id' => $tag['id'],
                    'name' => $tag['name'],
                ];
            }
        }

        return [
            'id' => $post->id,
            'title' => $post->title,
            'body' => $post->body,
            'tags' => $tags
        ];
    }
}