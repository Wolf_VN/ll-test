<?php

namespace App\Repositories;


interface ContractEloquent
{
    public function create(array $data);

    public function update(array $data);
    
    public function view($id);
    
    public function deleteById($id);
}