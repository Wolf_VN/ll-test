<?php

namespace App\Repositories;


use App\Post;
use App\Repositories\EloquentTag;
use App\Repositories\AbstractRepository;

class EloquentPost extends AbstractRepository implements ContractEloquent
{
    protected $eloquentTag;

    public function __construct(Post $post, EloquentTag $eloquentTag)
    {
        $this->model = $post;
        $this->eloquentTag = $eloquentTag;
    }

    public function create(array $data)
    {
        $postModel = parent::create($data);

        if ($postModel) {
            $tagIds = $this->savePostTag($data, $postModel);
            $postModel->tagIds = $tagIds;
        }

        return $postModel;
    }
    
    public function update(array $data)
    {
        $postModel = parent::update($data);

        if ($postModel) {
            $tagIds = $this->savePostTag($data, $postModel);
            $postModel->tagIds = $tagIds;
        }

        return $postModel;
    }

    /**
     * @param int $id
     * 
     * @return array
     */
    public function view($id)
    {
        $post = $this->getFirstBy('id', $id);

        if ( !$post) {
            return NULL;
        }

        $result = $post->toArray();
        $result['tags'] = [];
        foreach ($post->tags as $tag) {
            $result['tags'][] = $tag->toArray();
        }

        return $result;
    }

    private function savePostTag($data, $postModel)
    {
        $tagIds = [];
        if ( !empty($data['tags']) && is_array($data['tags'])) {

            $flipped = array_flip($data['tags']);
            // find tags name to get ID
            $tags = $this->eloquentTag->findByNames($data['tags']);
            $tagIds = array_values($tags);

            $newTags = array_diff_key($flipped, $tags);

            foreach ($newTags as $newTag => $tmp) {
                $this->eloquentTag->refreshModel();
                if ($tag = $this->eloquentTag->create(['name' => $newTag])) {
                    $tagIds[] = $tag->id;
                }
            }
        }

        $postModel->tags()->sync($tagIds);

        return $tagIds;
    }
    
    public function deleteById($id)
    {
        $post = $this->getFirstBy('id', $id);

        $post->tags()->detach();

        $post->delete();

        return true;
    }
    
    public function findByIds($ids)
    {
        return $this->model->whereIn('id', $ids)->get();
    }
}