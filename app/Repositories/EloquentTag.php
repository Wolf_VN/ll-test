<?php

namespace App\Repositories;


use App\Tag;
use App\Repositories\AbstractRepository;

class EloquentTag extends AbstractRepository implements ContractEloquent
{
    public function __construct(Tag $tag)
    {
        $this->model = $tag;
    }

    /**
     * @param string $name
     *
     * @return mixed
     */
    public function getIdByName($name)
    {
        return $this->model
                        ->where('name', $name)
                        ->select(['id'])
                        ->first();
    }

    /**
     * @param array $names
     *
     * @return array
     */
    public function findByNames($names)
    {
        $names = (is_array($names)) ? $names : [$names];

        $result = $this->model
            ->whereIn('name', $names)
            ->select(['id', 'name'])
            ->get();

        $result = $result->toArray();

        return array_pluck($result, 'id', 'name');
    }

    public function create(array $data)
    {
        return parent::create($data);
    }
    
    public function update(array $data)
    {
        return parent::update($data);
    }

    public function view($id)
    {
        return $this->getFirstBy('id', $id);
    }

    public function deleteById($id)
    {
        $tag = $this->getFirstBy('id', $id);

        $tag->posts()->detach();

        $tag->delete();

        return true;
    }
}