<?php

namespace App\Utils;

use Illuminate\Support\Facades\Mail;

class SendMail
{
    /**
     * @param string $view
     * @param string $to
     * @param string $subject
     * @param array $data
     */
    public function pushToQueueMail($view, $to, $subject, $data = [])
    {
        $queueName = config('mail.queue_name');

        Mail::queueOn($queueName, $view, $data,
            function ($message) use ($to, $subject, $data) {
                $message->to($to, !empty($data['name']) ? $data['name'] : '')->subject($subject);
            }
        );
    }
}