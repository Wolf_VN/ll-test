<?php

namespace App\Utils\Cache;

use Illuminate\Support\Facades\Redis;

class TagCache extends AbstractCache
{
    protected $pattern = "tag:%s:posts";

    /**
     * @param int|array $tagIds
     */
    public function listAllIntersectPostsByManyTag($tagIds)
    {
        if (empty($tagIds)) {
            return [];
        }

        $tagIds = is_array($tagIds) ? $tagIds : [$tagIds];

        $key = $this->interStoreWithTags($tagIds);

        return Redis::smembers($key);
    }

    /**
     * @param int|array $tagIds
     *
     * @return int
     */
    public function countIntersectPostByTagIds($tagIds)
    {
        if (empty($tagIds)) {
            return 0;
        }

        $tagIds = is_array($tagIds) ? $tagIds : [$tagIds];

        $key = $this->interStoreWithTags($tagIds);

        return Redis::scard($key);
    }

    private function interStoreWithTags(array $tagIds)
    {
        $key = $this->getKeyName($tagIds);

        $isExist = Redis::exists($key);

        if ( !$isExist) {
            $keys = [];
            foreach ($tagIds as $tagId) {
                $keys[] = $this->getKeyName($tagId);
            }

            // todo: need to set expiration is less than normal. Purpose: It will auto delete
            Redis::sinterstore($key, $keys);
        }

        return $key;
    }

    /**
     * @param int $postId
     * @param array $tagIds
     */
    public function removePostOutTag($postId, array $tagIds)
    {
        foreach ($tagIds as $tagId) {
            Redis::srem($this->getKeyName($tagId), $postId);
        }

        return true;
    }

    public function removeTag(array $tagIds)
    {
        foreach ($tagIds as $tagId) {
            $this->delete($tagId);
        }
    }
}