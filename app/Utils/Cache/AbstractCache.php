<?php
/**
 * Created by PhpStorm.
 * User: tungchung
 * Date: 8/3/16
 * Time: 21:26
 */

namespace App\Utils\Cache;

use Illuminate\Support\Facades\Redis;

class AbstractCache
{
    protected $pattern;

    /**
     * @param int|array $tagId
     *
     * @return string
     */
    protected function getKeyName($id)
    {
        $id = is_array($id) ? implode('-', $id) : $id;

        return sprintf($this->pattern, $id);
    }

    /**
     * @param int $key
     * @param string $val
     */
    public function keySet($key, $val)
    {
        Redis::set($this->getKeyName($key), $val);
    }

    /**
     * @param int $key
     * @return mixed
     */
    public function keyGet($key)
    {
        return Redis::get($this->getKeyName($key));
    }

    /**
     * Set value for key
     *
     * @param int|array $key
     * @param mixed $valIds
     */
    public function setSet($key, $valIds)
    {
        $keyName = $this->getKeyName($key);
        // remove
        Redis::del([$keyName]);
        // add new
        Redis::sadd($keyName, $valIds);
    }

    /**
     * @param int|array $key
     * @param mixed $valIds
     */
    public function setPush($key, $valIds)
    {
        // push
        Redis::sadd($this->getKeyName($key), $valIds);
    }

    /**
     * @param int|array $key
     */
    public function delete($key)
    {
        Redis::del([$this->getKeyName($key)]);
    }

    public function has($key)
    {
        return Redis::exists($this->getKeyName($key));
    }
}