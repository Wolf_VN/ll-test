<?php

namespace App\Utils\Cache;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redis;

class PostCache extends AbstractCache
{
    protected $pattern = "post:%s";

    protected $patternKeyList = "posts";

    protected $tagCache;

    public function __construct(TagCache $tagCache)
    {
        $this->tagCache = $tagCache;
    }

    public function buildOnePost(Model $model)
    {
        $this->keySet($model->id, $model);
        $this->addToListPosts($model);

        foreach ($model->tags()->get() as $tag) {
            $this->tagCache->setPush($tag->id, $model->id);
        }
    }

    /**
     * Get all ID of posts
     * @return mixed
     */
    public function getListPosts()
    {
        return Redis::zrange($this->patternKeyList, 0, -1);
    }
    
    public function addToListPosts(Model $model)
    {
        Redis::zadd($this->patternKeyList, [$model->id => $model->created_at->timestamp]);
    }

    public function removeOutListPosts($postId)
    {
        if ($postId instanceof Model) {
            $postId = $postId->id;
        }
        Redis::zrem($this->patternKeyList, $postId);
    }

    public function delete($key)
    {
        parent::delete($key);

        $this->removeOutListPosts($key);
    }
}