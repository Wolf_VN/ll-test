<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Utils\Cache\PostCache;
use App\Repositories\EloquentPost;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;


class PostJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $postIds;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($postIds)
    {
        $this->postIds = $postIds;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(EloquentPost $eloquentPost, PostCache $postCache)
    {
        foreach ($this->postIds as $postId) {
            $post = $eloquentPost->getById($postId, ['tags']);
            $postCache->onePost($post);
        }
    }
}
