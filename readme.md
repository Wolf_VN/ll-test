System requirement
================================
*(Follow Laravel requirement)*

- PHP >= 5.5.9
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension

*Needed service*

- Redis
- RabbitMQ

System setup
================================
###### Step 1: setup resource
  - Git clone with the repository link
  - Run composer ```composer install```
  - Generate key ```php artisan key:generate```
  - Migrate DB ``` php artisan migrate```

###### Step 2: Setup configuration in *.env* file
  - Change DB info
  - Change Redis info
  - Change Mail info
  - Change RabbitMQ info
   
###### Step 3: grant permission
  - run ```chmod -R 777 storage/```

###### Step 4: Setup to run some background jobs
  - Send mail ```php artisan queue:listen --queue=<MAIL_QUEUE_NAME>```
  - Update Cache for Post, and Tag ```php artisan queue:listen --queue=<QUEUE_BUILD_CACHE>```
  
###### Step 5: Change Admin email
  - in *.env* file, fill value for *\<MAIL_ADMIN\>*

